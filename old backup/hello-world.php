<html>
    <head>
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="/jquery.fancybox.css" type="text/css" />
        <style type="text/css">
            .col-md-3 {
                position: relative;
                border: 1px solid #ddd;
                background: #fff;
                margin: 0 0 20px;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
                
                min-height:120px;
            }
            .fancybox-nav {
    width: 60px;       
}

.fancybox-nav span {
    visibility: visible;
}

.fancybox-next {
    right: -160px;
}

.fancybox-prev {
    left: -160px;
}
.goaway img{
  display:none;  
}

        </style>
        
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="http://www.youtube.com/player_api"></script>
        <script type="text/javascript" src="/jquery.fancybox.js"></script>
        <script type="text/javascript">
        function onPlayerReady(event) {
    event.target.playVideo();
}

// Fires when the player's state changes.
function onPlayerStateChange(event) {
    // Go to the next video after the current one is finished playing
    if (event.data === 0) {
        $.fancybox.next();
    }
}

// The API will call this function when the page has finished downloading the JavaScript for the player API
function onYouTubePlayerAPIReady() {
    
    // Initialise the fancyBox after the DOM is loaded
    $(document).ready(function() {
        $(".fancybox")
            .attr('rel', 'gallery')
            .fancybox({
                openEffect  : 'none',
                closeEffect : 'none',
                nextEffect  : 'none',
                prevEffect  : 'none',
                padding     : 0,
                margin      : 50,
                beforeShow  : function() {
                    // Find the iframe ID
                    var id = $.fancybox.inner.find('iframe').attr('id');
                    
                    // Create video player object and add event listeners
                    var player = new YT.Player(id, {
                        events: {
                            'onReady': onPlayerReady,
                            'onStateChange': onPlayerStateChange
                        }
                    });
                }
            });
    });
    
}   $(document).ready(function(){
    $("#change2").click(function(){
        $(this).attr('disabled','disabled');
        $(this).addClass('disabled');
        $("#list").hide();
        $("#origin").html($("#list").html());
        $("#origin li").replaceWith(function () {
    return $('<div/>', {
        class: 'ui-sortable-handle col-md-3 ui-state-default',
        html: this.innerHTML
    });
});
$("#origin").show();

    });
                $("#change").click(function(){
                    $("#origin").hide();
                   $("#list").html($("#origin").html());
$("#list div").replaceWith(function () {
    return $('<li/>', {
        class: 'ui-sortable-handle ui-state-default list-group-item',
        html: this.innerHTML
    });
}); $("#list").show();
                });
                $("#list").sortable();
                $("#origin").sortable({
        start: function(event, ui) {
            var old = ui.item.index();

            ui.item.data('old', old);
        },
        update: function(event, ui) {
            var old = ui.item.data('old');
            var new_pos =  ui.item.index();
            
        }
    });
                $( "#origin" ).disableSelection();
                
            })
        </script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    </head>
    <body>
        
<div class="container">
    
    <div class="row">
        <ul id="list" class="list-group goaway"></ul>
    	<div id="origin" class="col-md-12">
    	    <div class= "col-md-3">
    	        <a class="fancybox fancybox.iframe" href="http://www.youtube.com/embed/L9szn1QQfas?enablejsapi=1&wmode=opaque"><img src="http://placehold.it/140x101" />
    	        Test</a>
    	    </div>
    		<div class= "col-md-3" >
    		    <a class="fancybox fancybox.iframe" href="http://www.youtube.com/embed/cYplvwBvGA4?enablejsapi=1&wmode=opaque"><img src="http://placehold.it/140x102" />
    	        test2</a>
    	    </div>
    	    <div class= "col-md-3" >
    	        <a class="fancybox fancybox.iframe" href="http://www.youtube.com/embed/BELUFOu9p5k?enablejsapi=1&wmode=opaque"><img src="http://placehold.it/140x103" />
    	        test</a>
    	    </div>
    	    <div class= "col-md-3" >
    	        <img src="http://placehold.it/140x104" />
    	    </div>
    	    <div class= "col-md-3" >
    	        <img src="http://placehold.it/140x105" />
    	    </div>
    	    <div class= "col-md-3" >
    	        <img src="http://placehold.it/140x106" />
    	    </div>
    	    <div class= "col-md-3" >
    	        <img src="http://placehold.it/140x106" />
    	    </div>
    	    <div class= "col-md-3 ui-state-default">
    	        <img src="http://placehold.it/140x106" />
    	    </div>
    	    <div class= "col-md-3 ui-state-default">
    	        <img src="http://placehold.it/140x106" />
    	    </div>
    	    <div class= "col-md-3 ui-state-default">
    	        <img src="http://placehold.it/140x106" />
    	    </div>
    	    <div class= "col-md-3 ui-state-default">
    	        <img src="http://placehold.it/140x106" />
    	    </div>
    	    <div class= "col-md-3 ui-state-default">
    	        <img src="http://placehold.it/140x106" />
    	    </div>
    	    <div class= "col-md-3 ui-state-default">
    	        <img src="http://placehold.it/140x106" />
    	    </div>
    	    <div class= "col-md-3 ui-state-default">
    	        <img src="http://placehold.it/140x106" />
    	    </div>
    	    <div class= "col-md-3 ui-state-default">
    	        <img src="http://placehold.it/140x106" />
    	    </div>
    	    <div class= "col-md-3 ui-state-default">
    	        <img src="http://placehold.it/140x106" />
    	    </div>
    	</div>
    	    	    <div class="row">
    	    <button id="change">List</button><button id="change2">Blocks</button></div>
	</div>
</div>
</body>
</html>