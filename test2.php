<html>
    <head>
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="main.css" type="text/css" />
    </head>
    <body>
        
<div class="container">
    
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="Search">Search:</label>
                <input type="text" class="form-control" id="search">
                <div id="search-res" class="list-group" style='overflow:scroll;max-height: 345px;display:none;'>
                    
                </div>
            </div>
        </div>
    	<div id="origin" class="col-md-12">

    		<div class="col-md-4 text-center"><a href="javascript:playTrailer('http://www.youtube.com/watch?v=XhohLI1Rupc','7267','Maduk - Got Me Thinking (ft. Veela)')" data-href="http://www.youtube.com/watch?v=XhohLI1Rupc"><img src="https://i.ytimg.com/vi/XhohLI1Rupc/mqdefault.jpg"><br>Maduk - Got Me Thinking (ft. Veela)</a></div>
    		<div class="col-md-4 text-center"><a href="javascript:playTrailer('https://www.youtube.com/watch?v=fX5USg8_1gA','7267','Maduk - Got Me Thinking (ft. Veela)')" data-href="https://www.youtube.com/watch?v=fX5USg8_1gA"><img src="https://i.ytimg.com/vi/XhohLI1Rupc/mqdefault.jpg"><br>Maduk - Got Me Thinking (ft. Veela)</a></div>
    	</div>
    	    <div class="row">
            	<div class="col-md-12">
            	       <div id="myElement">
            	                
            	       </div>
        	   </div>
    	   </div>
	</div>
</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
 <script type="text/javascript" src="https://content.jwplatform.com/libraries/Svy3pk2H.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
 <script type="text/javascript" src="main.js"></script>

</body>
</html>