<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Interfaces\TokenAuthenticatedInterface;
class CheckController extends Controller implements TokenAuthenticatedInterface
{
    /**
     * @Route("/user/check", name="checkUser")
     */
    public function checkAction(Request $request)
    {
        return new Response('',200);
    }
}
