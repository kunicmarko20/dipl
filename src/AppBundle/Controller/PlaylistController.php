<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Interfaces\TokenAuthenticatedInterface;
use AppBundle\Entity\Playlist;
use AppBundle\Entity\Song;
use AppBundle\Service\checkAccess;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;

class PlaylistController extends Controller implements TokenAuthenticatedInterface
{
    private $normalizer;
    private $serializer;
    private $metadata;
    public function __construct()
    {
        $this->metadata = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $this->normalizer = new ObjectNormalizer($this->metadata);
        $this->serializer = new Serializer(array($this->normalizer));
    }
    /**
     * @Route("/playlist/get", name="getPlaylist")
     */
    public function getAction(Request $request)
    {

        $result = $this->getDoctrine()->getManager()->getRepository('AppBundle:Playlist')->findBy(array('user' => $this->getUser()));
        
        return new JsonResponse($this->serializer->normalize($result, null, array('groups' => array('userPlaylist'))));
    }

    /**
     * @Route("/playlist/new", name="newPlaylist")
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $item = new Playlist();
            
        $name = $request->get('name');
        $private = $request->get('private') == 'true';
        $item->setUser($this->getUser());
        $item->setName($name);
        $item->setPrivate($private);
            
        $em->persist($item);
        $em->flush();
            
        return new Response('',200);
    }
    /**
     * @Route("/playlist/update", name="updatePlaylist")
     */
    public function updateAction(Request $request)
    {
        
        $playlist = $request->get('playlist');

        $item = $this->get('app.access')->checkAccess($playlist);
                
        $em = $this->getDoctrine()->getManager();
        $name = $request->get('name');
        $item->setName($name);
            
        $em->persist($item);
        $em->flush();
        $this->addFlash('success',"'$name' Added");
            
        return new Response('',200);
    }
     /**
     * @Route("/playlist/delete", name="deletePlaylist")
     */
    public function deleteAction(Request $request)
    {
        
        $item = $this->get('app.access')->checkAccess($request->get('id')); 
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        $em->clear();

        return new Response('',200);

    }
        /**
     * @Route("/playlist/reposition", name="repositionPlaylist")
     */
    public function repositionAction(Request $request)
    {
            
        $playlist = $request->get('playlist');
        
        $this->get('app.access')->checkAccess($playlist); 
        $em = $this->getDoctrine()->getManager();
        
        $songs = $request->get('songs');
        foreach($songs as $k => $song){
            $item = $em->getRepository('AppBundle:Song')->findOneBy(array('videoId' => $song,'playlist' => $playlist));
            $item->setPosition($k);
            $em->persist($item);
            $em->flush();
        }

        return new Response('',200);
    }
}
