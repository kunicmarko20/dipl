<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Playlist;
use AppBundle\Entity\Song;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;

class PublicController extends Controller
{
    private $normalizer;
    private $serializer;
    private $metadata;
    public function __construct()
    {
        $this->metadata = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $this->normalizer = new ObjectNormalizer($this->metadata);
        $this->serializer = new Serializer(array($this->normalizer));
    }
    
    
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('userbase.html.twig');
    }
    /**
     * @Route("/newest", name="newest")
     */
    public function getNewestAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $result = $em->getRepository('AppBundle:Song')->findBy(array(), array('id' => 'DESC'),6);
        foreach($result as $k => $r){
           $AJAXResponse['html'][$k]['id'] = $r->getId();
           $AJAXResponse['html'][$k]['vidid'] = $r->getVideoId();
           $AJAXResponse['html'][$k]['title'] = $r->getTitle();
        }
        
        return new JsonResponse($AJAXResponse,200);
    }
    /**
     * @Route("/profile/{name}", name="profile")
     */
    public function getProfileAction(Request $request,$name)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findBy(array('username' => $name));
        if(!$user){
            return new Response('User Not Found',404);
        }
        $result = $em->getRepository('AppBundle:Playlist')->findBy(array('user' => $user));
        
        return new JsonResponse($this->serializer->normalize($result, null, array('groups' => array('userPlaylist'))));
    }
}
