<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Interfaces\TokenAuthenticatedInterface;
use AppBundle\Entity\Playlist;
use AppBundle\Entity\Song;
use AppBundle\Service\checkAccess;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;

class SongController extends Controller implements TokenAuthenticatedInterface
{
    private $normalizer;
    private $serializer;
    private $metadata;

    public function __construct() {
        $this->metadata = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $this->normalizer = new ObjectNormalizer($this->metadata);
        $this->serializer = new Serializer(array($this->normalizer));
    }
     /**
     * @Route("/song/get", name="getSong")
     */
    public function getAction(Request $request)
    {
        $playlist = $request->get('playlist');
        


        $result = $this->getDoctrine()->getManager()->getRepository('AppBundle:Song')->findBy(array('playlist' => $playlist),array('position' => 'ASC','id'=>'ASC'));

        return new JsonResponse($this->serializer->normalize($result, null, array('groups' => array('playlistSongs'))));
    }
       /**
     * @Route("/song/new", name="newSongs")
     */
    public function addAction(Request $request)
    {
        
        $playlist = $request->get('playlist');

        $pl = $this->get('app.access')->checkAccess($playlist);
        
        $em = $this->getDoctrine()->getManager();
        $item = new Song();
        $title = $request->get('title');
        $videoid = $request->get('id');
            
        $item->setVideoId($videoid);
        $item->setPlaylist($pl);
        $item->setTitle($title);
        $item->setPosition(9999);
        $em->persist($item);
        $em->flush();

        return new Response('',200);
    }
     /**
     * @Route("/song/delete", name="deleteSong")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        
        $this->get('app.access')->checkAccess($request->get('playlist'));
        $em = $this->getDoctrine()->getManager();

        $item = $em->getRepository('AppBundle:Song')->findOneById($request->get('id')); 

        $em->remove($item);
        $em->flush();
        $em->clear();

        return new Response('',200);
    }
}
