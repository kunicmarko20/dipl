<?php

namespace AppBundle\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use AppBundle\Entity\User;

class AuthenticationHandlerSuccess implements AuthenticationSuccessHandlerInterface
{
    private $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }
    
    function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $rand = md5($token->getUser()->getUsername().mt_rand(10,100));
        $token->getUser()->setToken($rand);
        $this->container->get('doctrine')->getEntityManager()->flush();
        
        $fp = fopen($this->container->getParameter('skey_private_key_path'), "r");
        $cert = fread($fp, 8192);
        fclose($fp);
        $res = openssl_pkey_get_private($cert,'12345');
        
        openssl_private_encrypt($rand,$sig,$res); 

        $AJAXResponse['token'] = base64_encode($sig);
        $response = new Response(json_encode($AJAXResponse));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    public function onAuthenticationFailure(Request $request, TokenInterface $token){
        return new Response('',403);
    }
}