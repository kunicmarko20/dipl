<?php 

namespace AppBundle\Service;

use AppBundle\Playlist;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Doctrine\ORM\EntityManager;

class checkAccess {
    
    protected $em;
    protected $user;

    public function __construct(EntityManager $em,$user) {
        $this->em = $em;
        $this->user = $user;
    }
    
    public function checkAccess($playlist){
        $pl = $this->em->getRepository('AppBundle:Playlist')->findOneBy(array('id' => $playlist, 'user' => $this->user));
        if(!$pl){
            throw new AccessDeniedHttpException('You don\'t have access to do that');
        }
        return $pl;
    }
}