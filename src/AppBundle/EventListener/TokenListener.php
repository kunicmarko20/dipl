<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use AppBundle\Interfaces\TokenAuthenticatedInterface;

class TokenListener
{
    protected $em;
    private $container;

    public function __construct(EntityManager $em,Container $container) {
        $this->container = $container;
        $this->em = $em;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }
        if ($controller[0] instanceof TokenAuthenticatedInterface) {

            if( !$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY') ){
                throw new AccessDeniedHttpException('This action needs a valid token!');
            }
            $sig = $event->getRequest()->headers->get('x-auth-token');
            $fp = fopen($this->container->getParameter('skey_public_key_path'), "r");
            $cert = fread($fp, 8192);
            fclose($fp);
            $pub_key = openssl_pkey_get_public($cert);
            $signature = base64_decode($sig);
            if(openssl_public_decrypt($signature,$token,$pub_key)){
                $userExists = $this->em
                    ->getRepository('AppBundle:User')
                    ->findOneBy(array('token' => $token));
                if(!$userExists){
                   throw new AccessDeniedHttpException('This action needs a valid token!');
                }
            }else {
                throw new AccessDeniedHttpException('This action needs a valid token!');
            }
        }
    }
}
