var app = angular.module('ytApp', ['ngRoute']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/profile/:name',
            {
                controller: 'PlaylistController',
                templateUrl: '/app/partials/playlist.html'
            })
        .when('/login',
            {
                controller: 'LoginController',
                templateUrl: '/app/partials/login.html',
                resolve: {
                    factory: checkLogin
                }
            })
        .when('/',
            {
                controller: 'IndexController',
                templateUrl: '/app/partials/index.html'
            })
        .otherwise({ redirectTo: '/' });
});

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.post['X-Auth-Token'] = localStorage.getItem("token");
//    $httpProvider.interceptors.push(function($q, $rootScope, $location) {
//  return {
//    'responseError': function(response) {
//        if(response.status == 403){
//            $location.path("login");
//            $rootScope.flash = {danger:"Please Login"};
//        }
//       return $q.reject();
//    }
//  };
//});
}]);

var checkRouting= function ($rootScope, $q, $location, $http) {
//    var time =  Math.floor(sessionStorage.getItem('time')) + 600;
//    var current = new Date().getTime() / 1000;
//    if(time < current){
        var deferred = $q.defer();
            $http.post("user/check")
                .success(function (response) {
                    deferred.resolve(true);
                    $rootScope.logshow = false;
                })
                .error(function (response) {
                    $rootScope.logshow = true;
                    localStorage.removeItem("token");
                    deferred.reject();
                    $location.path("login");
                    $rootScope.flash = {danger:"Please Login"};
                });
//        sessionStorage.setItem('time',current);
        return deferred.promise;
//    }
};
var checkLogin= function ($rootScope, $q, $location, $http) {
    var deferred = $q.defer();
        if($rootScope.logshow || !localStorage.getItem("token")){
          deferred.resolve(true);
          return deferred.promise;
        }
        deferred.reject();
        $location.path("/");
        return deferred.promise;
};

app.controller('AppController', function ($scope) {
    $scope.isAuth = function(){
        return localStorage.getItem("token") ? localStorage.getItem("username"): '';
    };
    $scope.showhide = function(event){
        $('.'+event).removeClass(event);
    }
    $scope.Logout = function(){
      localStorage.clear();
    }
});
