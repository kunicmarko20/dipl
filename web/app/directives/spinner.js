//This directive adds custom animations to views as they enter or leave a screen
//Note that AngularJS 1.1.4 now has an ng-animate directive but this one can be used when you 
//want complete control or when you can't use that version of AngularJS yet
app.directive('loading',   ['$http' ,function ($http)
 {
     return {
         restrict: 'A',
         template: '<div class="loading-spiner" id="loader"><img src="http://www.nasa.gov/multimedia/videogallery/ajax-loader.gif" /> </div>',
         link: function (scope, elm, attrs)
         {
             scope.isLoading = function () {
                 return $http.pendingRequests.length > 0;
             };

             scope.$watch(scope.isLoading, function (v)
             {
                 if(v){
                     elm.show();
                 }else{
                     elm.fadeOut(1000);
                 }
             });
         }
     };
 }])