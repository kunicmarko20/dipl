var pl;

app.controller('PlaylistController', function ($rootScope,$scope, $http, $location, $compile, $templateCache, $route, $routeParams) {


    var currentPageTemplate;
     init();
    function init() {
        $('.modal-sm .modal-footer button:first').attr("id","btn-submit-playlist");
        $('#lgModal .modal-footer button:first').attr("id","btn-save-playlist").text('Save');
        $('#lgModal .pull-left').remove();
        $('#btn-play-playlist').remove();
        $('#lgModal .modal-footer').prepend('<button type="button" class="pull-left btn btn-primary" id="btn-add-song" ng-click="addSong($event);" data-toggle="modal" data-target="#smModal"><span class="glyphicon glyphicon-plus"></span> Add new Song</button><button ng-click="delPlaylist($event);" id="btn-del-playlist" type="button" class="btn btn-danger pull-left">Delete playlist</button><button type="button" class="btn btn-success" id="btn-play-playlist" ng-click="playPlaylist();"><span class="glyphicon glyphicon-play"></span> Play</button>');
        $compile($('#btn-save-playlist'))($scope);
        $compile($('#btn-submit-playlist'))($scope);
        $compile($('#btn-del-playlist'))($scope);
        $compile($('#btn-add-song'))($scope);
        $compile($('#btn-play-playlist'))($scope);
        getPlaylists();
        currentPageTemplate = $route.current.templateUrl;

    }

    function getPlaylists(){
        $http({url: 'profile/'+$routeParams.name,method: "POST"}).then(function successCallback(response) {
            $scope.userPlaylist = response.data;
            $scope.showdata = true;
  }, function errorCallback(response) {
      console.clear();
    $location.path("/");
  });}

    $scope.newPlaylist = function(){
        $('.modal-sm .modal-title').text('Add New Playlist');
        $('.modal-sm .modal-body').html('<div class="form-group text-center"></div>');
        $('.modal-sm .modal-body .form-group').append('<input type="text" class="form-control" placeholder="Playlist Name" id="playlist-name"/><br/>Private Playlist : <input type="checkbox" id="private-playlist"/>');
        $('#btn-submit-playlist').show();
    }
    $scope.delSong = function(event,id){
        $(event.target).parent().toggle( "clip", function() { $(event.target).remove(); });
        $http({
                  method: "POST",
                  url: "song/delete",
                  data: $.param({'id': id,'playlist':pl}),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
    }
    $scope.editTitle = function(event = false){
        if(event){
           $('#lgModal .modal-header span').remove();
           $('#lgModal .modal-header input').before('<h4 class="modal-title pull-left-title">'+$('#lgModal .modal-header input').val()+'</h4> <span role="button" id="edit-playlist-title" ng-click="editTitle()" class="glyphicon glyphicon-pencil position-pencil"></span>').remove();
          $compile($('#edit-playlist-title'))($scope);
        }else{
          $('#lgModal .modal-header h4').before('<input type="text" id="edit-title-input" value="'+$('#lgModal .modal-title').text()+'"/>').remove();
          $('#lgModal .modal-header span').before(' <span role="button" id="save-playlist-title" ng-click="saveTitle()" class="glyphicon glyphicon-floppy-disk"></span> <span role="button" id="edit-playlist-title" ng-click="editTitle(true)" class="glyphicon glyphicon-remove"></span>').remove();
          $compile($('#edit-playlist-title'))($scope);
          $compile($('#save-playlist-title'))($scope);
        }

    }
    $scope.saveTitle = function(){
        var title = $('#lgModal .modal-header input').val();
        if(title != ''){
            $http({
                  method: "POST",
                  url: "playlist/update",
                  data: $.param({'name': title,'playlist':pl}),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                 success(function() {
                    $templateCache.remove(currentPageTemplate);
                    $route.reload();
                    $('#lgModal').modal('toggle');
            });

        }
    }
    $scope.openPlayist = function(event){
                $('#lgModal .modal-header span').remove();
                $('#lgModal .modal-header input').before('<h4 class="modal-title pull-left-title">'+$('#lgModal .modal-header input').val()+'</h4>').remove();

                $('#lgModal .modal-title').addClass('pull-left-title').text($(event.target).parent().attr('title')).after('<span role="button" id="edit-playlist-title" ng-click="editTitle();" class="position-pencil glyphicon glyphicon-pencil"></span>');
                $compile($('#btn-reload-playlist'))($scope);
                $compile($('#edit-playlist-title'))($scope);
                $scope.playlist = pl = $(event.target).parent().data('pid');
                $scope.loadSongs(true);
                $('#btn-del-playlist').attr('data-playlist',$scope.playlist);
    }
    $scope.loadSongs = function(open = false){
        $http({
                  method: "POST",
                  url: "song/get",
                  data: $.param({'playlist': $scope.playlist}),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                 success(function(response) {
                    var style = 'max-height: 500px;';
                    if(response){
                        if(response.length > 6){
                            style = 'overflow-y:scroll;max-height: 500px;';
                        }
                        $('#lgModal .modal-body').html('<div class="container" style="'+style+'"><div id="origin" class="col-md-12"></div></div>');

                        $.each( response, function( key, s ) {
                            var title = s.title;
                            if(title.length > 45) {
                                title = title.substring(0,44)+"...";
                            }
                          $('#origin').append('<div class="col-md-4 text-center"><button type="button" ng-click="delSong($event,'+s.id+');" class="close btn-del-song">&times;</button><a href="javascript:void(0)" onclick="playTrailer(\'http://www.youtube.com/watch?v='+s.videoId+'\',\''+s.title+'\',this)" title="'+s.title+'" data-href="http://www.youtube.com/watch?v='+s.videoId +'"><img src="https://i.ytimg.com/vi/'+s.videoId +'/mqdefault.jpg"><br>'+ title +'</a></div>');
                        });
                        $compile($('.btn-del-song'))($scope);
                        $.getScript('/js/sort.js');
                    }
                    open && $('#lgModal').modal('toggle');
                });
    }
    $scope.playPlaylist = function(){
        $('#now-playing-songs').html($('#origin').children().clone());
        $('#now-playing-songs .col-md-4 a').data('in-playlist',true);
        $('#lgModal').modal('hide');
        if($('#now-playing-songs').children().length > 6){
            $("#now-playing-songs").css({"max-height":"500px", "overflow-y":"scroll"});
        }else {
            $("#now-playing-songs").removeAttr("style")
            $("#now-playing-songs").css({"max-height":"500px"});
        }
        $.getScript('/js/sort.js');
        $('#now-playing-songs .col-md-4:first a')[0].click();
        setTimeout(function(){ jwplayer("myElement").play(); }, 100);
        $('.my-custom-player-button').show();

        $(".btn-del-song").click(function(){
            $(this).parent().toggle( "clip", function() { $(this).remove(); });
        });
    }
    $scope.smModal = function(event){
            var playlistn = $('#playlist-name').val();
            var privateplaylist = $('#private-playlist').is(":checked");
            if(playlistn != "" && playlistn != null){
                $http({
                  method: "POST",
                  url: "playlist/new",
                  data: $.param({'name': playlistn,'private':privateplaylist}),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                 success(function(response) {
                    $('#smModal').modal('toggle');
                    $templateCache.remove(currentPageTemplate);
                    $route.reload();
                });
            }
    }
    $scope.lgModal = function(event){
        var pls = $('#btn-del-playlist').data('playlist');
        var uppos = [];
        $("#origin .col-md-4 a").each(function( ) {
            uppos.push($(this).data('href').split('=')[1]);
        });
        if (uppos.length > 0) {
            $http({
                  method: "POST",
                  url: "playlist/reposition",
                  data: $.param({'playlist': pls,'songs': uppos}),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                 success(function(response) {
                });
        }

    }
    $scope.addSong = function(event){
        $('.modal-sm .modal-title').text('Add New Song');
        $('.modal-sm .modal-body').html('<div class="form-group"></div>');
        $('.modal-sm .modal-body .form-group').append('<label for="Search">Search:</label><input type="text" class="form-control" id="search"/>');
        $('.modal-sm .modal-body').append('<div id="search-res" class="list-group" style="overflow-y:scroll;max-height: 345px;display:none;"></div>');
        $('#btn-submit-playlist').hide();
        $.getScript('/js/search.js');
    }
    $scope.delPlaylist = function(event){
                    var id = $(event.target).data('playlist');
                    $http({
                      method: "POST",
                      url: "playlist/delete",
                      data: $.param({'id': id}),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function(response) {
                        $('#lgModal').modal('toggle');
                        $templateCache.remove(currentPageTemplate);
                        $route.reload();
                    });

    }
    $('#lgModal').on('hidden.bs.modal', function () {
          $('#lgModal .modal-body').html('');
    });
    $('#smModal').on('hidden.bs.modal', function () {
          $('.modal-sm .modal-body').html('');
          if($('#smModal .modal-title:contains("Song")').length){
            $scope.loadSongs();
          }

    });
});

app.controller('LoginController', function ($scope, $http, $location) {

    localStorage.removeItem("token");

    $scope.Login = function(){
        var username = $("#username").val();
        var password = $("#password").val();
        if(username != '' && password != ''){
            $http({
                              method: "POST",
                              url: "login",
                              data: $.param({'_username':username,'_password':password}),
                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).success(function(response) {
                                localStorage.setItem("token", response.token);
                                localStorage.setItem("username",username);
                                console.clear();
                                $http.defaults.headers.post['X-Auth-Token'] = response.token;
                                $location.path("/profile/"+username);
                            }).error(function () {
                            localStorage.clear();
                            $scope.flash = {danger:"Bad credentials"};
                            $("#password").val('');
                        });
            }
        }


});

app.controller('IndexController', function ($scope, $http, $location) {

    getNewest();

    function getNewest(){
        $http({url: 'newest',method: "POST"}).then(function(response) {
            $.each( response.data.html, function( key, s ) {
                var title = s.title;
                if(title.length > 45) {
                    title = title.substring(0,44)+"...";
                }
                $('#origin').append('<div class="col-md-4 text-center"><a href="javascript:void(0)" onclick="playTrailer(\'http://www.youtube.com/watch?v='+s.vidid+'\',\''+s.title+'\',this)" title="'+s.title+'" data-href="http://www.youtube.com/watch?v='+s.vidid +'"><img src="https://i.ytimg.com/vi/'+s.vidid +'/mqdefault.jpg"><br>'+ title +'</a></div>');
                $scope.index = true;
            });
        });
    }
    $scope.playNewest = function(){
        $scope.playPlaylist();
    }
});