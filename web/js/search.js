jQuery(document).ready(function(){
    jQuery('#search').keyup(function(){
        var search = jQuery(this).val();

        if(search.length >= 3){
            jQuery("#search_result").empty();
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=25&fields=items(id%2FvideoId%2Csnippet(thumbnails(default%2Cmedium)%2Ctitle))&q="+search+"&type=video&key=AIzaSyDhCL06nyVIsukuFCd0ygwpkAZBi6ExbzI",
                "method": "GET",
                "headers": {
                    "cache-control": "no-cache",
                    "postman-token": "1e6fb448-af96-50e9-2891-53966a09c6e3"
                }
            }

            jQuery.ajax(settings).done(function (response) {
                var result = '';
                var i = 0;
                if( response.items.length != 0){
                    response.items.forEach(function(entry) {
                    var title = entry.snippet.title;
                    if(title.length > 40) {
                        title = title.substring(0,39)+"...";
                    }
                    result += "<a data-title='"+entry.snippet.title+"' data-vid='"+entry.id.videoId+"' data-img='"+entry.snippet.thumbnails.medium.url+"' class='list-group-item' href='"+i+"'><img src='"+entry.snippet.thumbnails.default.url+"' /><span> "+title+"</span></a>";
                    });
                    jQuery("#search-res").show().html(result);

                }
            });

        } else {
            jQuery("#search-res").hide().empty();
        }
    });

    jQuery('#search-res').on('click','.list-group-item',function(e){
        e.preventDefault();
        var vid = $(this).data('vid')
        var title = $(this).data('title');
        var img = $(this).data('img');
        var num = jQuery(this).attr('href');
        var href = "http://www.youtube.com/watch?v="+vid;
        var a = "playTrailer('"+href+"','"+title+"')";
        $.ajax({
            url:'new-song',
            method:'POST',
            data:{title:title,playlist:pl,id:vid},
            headers: {'X-Auth-Token': localStorage.getItem("token")},
            success: function(){
                jQuery('#origin').append('<div class="col-md-4 text-center"><button type="button" class="close btn-del-song">&times;</button><a href="javascript:void(0)" onclick="'+a+'" data-href="'+href+'"></a></div>');
                jQuery('.col-md-4:last a').html('<img src="'+img+'" /><br/>'+title);
                jQuery("#search-res").hide().empty();
            },
            error: function(){
                $('#lgModal').modal('toggle');
                $('#smModal').modal('toggle');
                window.location.replace("/app_dev.php/#/login");
            }
        });

    });
})